<?php

/**
 * Implementation of hook_node_info().
 */
function volunteer_registration_feature_node_info() {
  $items = array(
    'volunteer_registration' => array(
      'name' => t('Volunteer Registration'),
      'module' => 'features',
      'description' => t('An individual registration for a volunteer timeslot.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
    'volunteer_timeslot' => array(
      'name' => t('Volunteer Timeslot'),
      'module' => 'features',
      'description' => t('A timeslot for volunteers which can be repeated'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function volunteer_registration_feature_views_api() {
  return array(
    'api' => '2',
  );
}
